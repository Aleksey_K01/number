package ru.kav.number;

public class GuessGame {
    public void startGame() {
        Player p1 = new Player();
        Player p2= new Player();
        Player p3 = new Player();
        int guessp1 = 0;
        int guessp2 = 0;
        int guessp3 = 0;
        boolean p1isRight = false;
        boolean p2isRight = false;
        boolean p3isRight = false;
        int targetNumber = (int) (Math.random() * 10);
        System.out.println("Я думаю о числе от 0 до 9 ...");
        while(true) {
            System.out.println("Число, которое нужно угадать " + targetNumber);

            p1.guess();
            p2.guess();
            p3.guess();

            guessp1 = p1.number;
            System.out.println("Первый игрок загадал " + guessp1);
            guessp2 = p2.number;
            System.out.println("Второй игрок загадал " + guessp2);
            guessp3 = p3.number;
            System.out.println("Третий игрок загадал " + guessp3);

            if (guessp1 == targetNumber) {
                p1isRight = true;
            }
            if (guessp2 == targetNumber) {
                p2isRight = true;
            }
            if (guessp3 == targetNumber) {
                p3isRight = true;
            }

            if (p1isRight || p2isRight || p3isRight)
            {
                System.out.println("У нас победитель!");
                System.out.println("Первый игрок был прав? " + p1isRight);
                System.out.println("Второй игрок был прав? " + p2isRight);
                System.out.println("Третий игрок был прав? " + p3isRight);
                System.out.println("Игра закончена");
                break;
            }
            else
            {
                System.out.println("Игрокам придется повторить попытку.");
            }
        }
    }
}
